#! /usr/bin/gawk -f

$1 != evid { emit() }
END { emit() }

BEGIN {
    nDET=0
    DET[nDET++] = 1 
    DET[nDET++] = 11
    DET[nDET++] = 2
    DET[nDET++] = 22
    DET[nDET++] = 3
    DET[nDET++] = 33
    DET[nDET++] = 4
    DET[nDET++] = 44
    DET[nDET++] = 5
    DET[nDET++] = 55
    DET[nDET++] = 6
    DET[nDET++] = 7
    DET[nDET++] = 8
}

function emit( p, d) {
    if (!evid) return
    p = "?"
    if (Particle["alpha"]) p="α"
    else if (Particle["proton"]) p="p"
    else if (Particle["e-"]) p="e¯"
    else for (d in Particle) p=d
	     
    printf "%u %g %s ", evid, Eprim, p
    for (d=0; d<nDET; d++) printf " %g", Edep[DET[d]]+0
    printf "\n"
    delete Edep
    delete Particle
    evid = 0
}

/^[0-9]/ {
    evid = $1
    Eprim = $3
    Edep[$2] += $4
    Particle[$5] ++
}
