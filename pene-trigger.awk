#! /usr/bin/gawk -i

BEGIN {
    mMIP      = 0.1
    mALPHA    = 0.500
    mALPHA2   = 1.040
    mELECTRON = 0.250
    rELECTORN = 5/3
    mPROTON   = 0.200
    lratio    = 3
}

function isPene() {
    if ($6<mMIP || $7<mMIP || $8<mMIP || $9<mMIP || $10<mMIP || $11<mMIP) return 0
    m2 = $6<$7 ? $6 : $7
    if (lratio*m2 < $6 || lratio*m2<$7) return 0 
    m4 = $10<$11 ? $10 : $11
    if (lratio*m4 < $10 || lratio*m4<$11) return 0 
    LET3 = $8<$9 ? $8 : $9
    DIR = m4<m2
    if (!DIR) {
	BGO1 = $14
	BGO2 = $15
	LET2 = m2
	LET4 = m4
    } else {
	BGO1 = $15
	BGO2 = $14
	LET2 = m4
	LET4 = m2
    }
    Eprim = $2
    Pprim = $3
    if (LET2 > mALPHA  && LET3 > mALPHA && LET2+LET3 > mALPHA2) return C=3
    if (LET2 < mPROTON && LET4 > mELECTRON && LET4 > rELECTORN*LET2) {
	if (LET3 > mELECTRON) return C=2
	return C=4
    }
    return C=1
}

function lratio_Test() {
    r2 = ($6+$7)/m2 - 1
    r4 = ($10+$11)/m4 - 1
    print $3, $2, C, DIR ? r4:r2, DIR ? r2:r4
}
