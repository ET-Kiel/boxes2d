#! /usr/bin/python3

from sys import argv, stdin, stdout, stderr
from math import log10, pi as π
deg=π/180
import numpy
from numpy import sqrt, sin, cos, tan, array, zeros, empty, cross, newaxis, arange
from numpy.linalg import solve, det, norm
from getopt import getopt

def scale(x=1., y=1., z=1.):
    return array(((x,0,0),
                  (0,y,0),
                  (0,0,z)))

def rotate(φ, axis=2):
    m = scale()
    c = cos(φ)
    s = -sin(φ)
    for i in range(3):
        if i != axis:
            m[i,i]=c
            for j in range(3):
                if j != i and j != axis:
                    m[i,j] = s
                    s = -s
    return m

def vector(x=0., y=0., z=0.):
    return array([[x],[y],[z]])

def vtuple(v):
    return tuple(v[:,0])

def length(v):
    return sqrt(v.T @ v)[...,0,0]

class cut:
    index=[0]
    
    def __init__(self, o, v, boxes=(1,), w=vector(z=1.0)):
        if isinstance(o, float):
            o = vector(y=o)
        if isinstance(o, tuple):
            if len(o)<2:
                o = vector(y=o[0])
            else:
                o = vector(*o)
        if isinstance(v, float):
            v = vector(x=1.0, y=v)
        if isinstance(v, tuple):
            if len(v)<2:
                v = vector(x=1.0, y=v[0])
            else:
                v = vector(*v)
        
        self.idx = self.index[0]
        self.index[0] += 1
        self.o = o
        self.v = v
        self.w = w
        self.boxes=boxes

    def __repr__(self):
        return "cut[%d](o=(%g,%g,%g), v=(%g,%g,%g), w=(%g,%g,%g), boxes=%s)" % (
            (self.idx,)+vtuple(self.o)+vtuple(self.v)+vtuple(self.w)+(self.boxes,))

class boxes(list):
    
    def build(self):
        self.nboxes = max([len(c.boxes) for c in self])
        self.o = array([c.o for c in self])
        self.v = array([c.v for c in self])
        self.w = array([c.w for c in self])
        self.b = zeros((len(self),self.nboxes))
        for i,c in enumerate(self):
            if 2 <= len(c.boxes) < self.nboxes:
                self.b[i,0:len(c.boxes)] = c.boxes
                self.b[i,len(c.boxes):] = c.boxes[-1]
            else:
                self.b[i]=c.boxes
        self.n = cross(self.v, self.w, axis=1).transpose((0,2,1))

    def distance(self, p):
        return (self.n @ (p[...,newaxis,:,:] - self.o))[...,0] * self.b

    def inside(self, p, eps=0):
        return numpy.logical_and.reduce(self.distance(p) >= eps, axis=-2)

    def hits(self, p):
        return [tuple(bb).index(True)
                if True in bb
                else None
                for bb in self.inside(p)]

    def hit(self, *a, **aa):
        if a and isinstance(a[0], numpy.ndarray):
            v = a[0]
        else:
            v = vector(*a, **aa)
        return self.hits(array([v]))[0]

    def intersect2d(self, c, eps=0.0001):
        A = empty(c.v.shape[:-2]+self.v.shape[:-2]+(3,3))
        A[..., 0] = self.v[..., 0]
        A[..., 1] = self.w[..., 0]
        A[..., 2] = c.v[..., newaxis, :, 0]
        b = self.o - c.o[..., newaxis, :, :]
        s = abs(det(A))>eps
        x = solve(A[s], b[s])
        bb = (b - self.o)[s]
        p = x[...,2:,:] * A[s][..., 2, newaxis] - bb
        #print(s[...,0], file=stderr)
        #print(x[...,2,:], file=stderr)
        #print(bb[...,0], file=stderr)
        #print(A[s][...,2], file=stderr)
        #print(p[...,0], file=stderr)
        pp = list(p)
        pp.sort(key=lambda x: tuple(x[:,0]))
        p = array(pp)
        # p.sort(axis=0) sorts the columns individually :-(
        #print(p[...,0], file=stderr)
        u = empty(p.shape[:-2]+(1,), dtype=bool)
        u[0]=True
        u[1:] = norm(p[1:]-p[:-1], axis=-2) > eps
        return p[u[...,0]]

    def corners(self, eps=0.00001):
        p = self.intersect2d(self)
        s = self.inside(p, eps=-eps)
        cc = [p[ss] for ss in s.T]
        ccc = []
        for c in cc:
            ccc.append(c)
            nn = c.shape[0]
            if nn<3:
                continue
            # all vectors between corners
            dd = c[:,newaxis]-c
            # all cross products between vectors
            d = cross(dd[:,:,newaxis], dd, axis=-2)[...,2,0]
            # select colinear vectors
            s = abs(d) < eps
            # removing all duplicates and diagonals.
            tri=numpy.triu_indices(nn)
            s[tri]=False
            s.transpose(0,2,1)[tri]=False
            s.transpose(1,2,0)[tri]=False
            # indices of colinear corners
            ss=s.nonzero()
            if ss[0].shape[0]:
                #print(len(ccc), "colinearity\n", ss, file=stderr)
                # keep the pair with the longest distance
                d0 = norm(dd[ss[1:]], axis=-2)[...,0]
                d1 = norm(dd[(ss[0],ss[2])], axis=-2)[...,0]
                d2 = norm(dd[ss[:2]], axis=-2)[...,0]
                iii = empty(d0.shape[0], dtype=int)
                iii[numpy.logical_and(d0>=d1, d0>=d2)] = 0
                iii[numpy.logical_and(d1>=d0, d1>=d2)] = 1
                iii[numpy.logical_and(d2>=d0, d2>=d1)] = 2
                ii = tuple(array(ss)[(iii,arange(d0.shape[0]))])
                #print(c[(ii,...)], file=stderr)
                ii = tuple((i for i in range(c.shape[0]) if i not in ii))
                c = c[(ii,...)]
                ccc[-1] = c
                
            nn = c.shape[0]
            if nn<=3:
                continue
            dd = c[:,newaxis]-c
            d = cross(dd[:,:,newaxis], dd, axis=-2)[...,2,0]

            ii=[0,1]
            for i in range(2,nn):
                for j in range(len(ii)):
                    if d[ii[j-1],i,ii[j]] < -eps:
                        continue
                    ii[j:j] = [i]
                    break
            c = c[(ii,...)]
            ccc[-1] = c
        return ccc

    def gnuplot(self, counts=None, f=stdout):
        if counts is None:
            counts=tuple(range(self.nboxes))
        p = self.corners()
        for i,pp in enumerate(p):
            if not pp.shape[0]:
                continue
            for ppp in pp:
                print("%g %g %g %g" % (tuple(ppp[:,0])+(counts[i],)), file=f)
            print("%g %g %g %g\n\n" % (tuple(pp[0,:,0])+(counts[i],)), file=f)

    def gnuplot_labels(self, f):
        p = self.corners()
        for i,pp in enumerate(p):
            if not pp.shape[0]:
                continue
            ppp = pp.mean(axis=0)
            print("set label %d \"%d\" at %g,%g front tc lt %d font \",20\""
                  % (i+1,i,ppp[0,0],ppp[1,0], i), file=f)

        
macros={}

def aval(v):
    vv=""
    while v!=vv and v.find("$") >= 0:
        vv=v
        for m in macros:
            v = v.replace(m, macros[m])
    v = v.replace("°", "*deg")
    return eval(v)

def avec(v):
    v = v.strip().replace(",", " ")
    return tuple([aval(vv) for vv in v.split()])

short_opt = "c:p:U:D:LEGRx:y:z:w:"
long_opt = [
    "cut=", "point=",
    "run", "corners", "gnuplot", "labels",
    "logx", "logy", "logz", "ix=", "iy=", "iz=", "iw=",
    "define=", "replace=", "undefine="]


args = argv[1:]
options = []
while args:
    oo, aa = getopt(args, short_opt, long_opt)
    args = []
    options.extend(oo)
    if aa:
        fn = aa[0]
        with open(fn) as f:
            for l in f:
                l=l.strip()
                if not l:
                    continue
                if l[0]=='-':
                    args.append(l)
                elif l[0]=='@':
                    args.append(l[1:])
                elif l[0] != "#":
                    args.append("--"+l)
        args.extend(aa[1:])

world = boxes()
points = []
do_corners = False
do_gnuplot = False
do_labels = False
do_run = False
ix = 0
iy = 1
iz = None
iw = None
convx = lambda x:x
convy = convx
convz = convx
column_offset = -1

for o,v in options:
    
    if o in "-c --cut":
        world.append(cut(*tuple([avec(vv) for vv in v.split(";")])))
        
    if o in "-p --point":
        points.extend([vector(*tuple(avec(vv))) for vv in v.split(";")])

    if o in "-U --undefine -D --define --replace":
        vv=v.split("=",1)
        k = "${"+vv[0]+"}"
        if o in "-U --undefine":
            if k in macros:
                stderr.write("undefine macro %s=%s\n" % (k,macros[k]))
                del macros[k]
        elif o in "--replace" or not k in macros:
            if k in vv[1]:
                raise ValueError("recursive macro %s" % v)
            macros[k]=vv[1]
            stderr.write("define macro %s=%s\n" % (k,vv[1]))

    if o in "-x --ix":
        ix = aval(v) + column_offset
    if o in "-y --iy":
        iy = aval(v) + column_offset
    if o in "-z --iz":
        iz = aval(v) + column_offset
    if o in "-w --iw":
        iw = aval(v) + column_offset

    if o in "--logx":
        convx = log10
    if o in "--logy":
        convy = log10
    if o in "--logz":
        convz = log10
        
    if o in "-E --corners":
        do_corners=True
    if o in "-G --gnuplot":
        do_gnuplot=True
    if o in "-L --labels":
        do_labels=True
    if o in "-R --run":
        do_run=True
 
world.build()

counts=None
if do_run or points:
    counts=[0.0] * world.nboxes

def count_point(p, w=1.0, l=""):
    h = world.hit(p)
    if h is not None:
        counts[h] += w
    if h is None:
        h = -1
    if not do_gnuplot:
        print("%g %g %g %d %s" % (tuple(p[:,0])+(h,l)))

list(map(count_point, points))

if do_corners:
    print(repr(world.corners()), file=stderr)

if do_run:
    for l in stdin:
        l=l.strip()
        if not l or l[0] == "#":
            continue
        p=vector()
        ll = l.split()
        n = len(ll)
        if n<ix or n<iy or iz is not None and n<iz or iw is not None and n<iw:
            continue
        w = 1.0
        try:
            p[0,0]=convx(float(ll[ix]))
            p[1,0]=convy(float(ll[iy]))
            if iz is not None:
                p[2,0]=convz(float(ll[iz]))
            if iw is not None:
                w = float(ll[iw])
        except ValueError as e:
            print(l, repr(e), file=stderr)
            continue
        count_point(p, w, l)

if do_labels:
    world.gnuplot_labels(stdout)
        
if do_gnuplot:
    world.gnuplot(counts)
