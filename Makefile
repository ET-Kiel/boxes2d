
SDIR=/data/tr1/sjensen/sensi/output/beams_hits/dummy_2

mc/beams_hits-2022-03-15.edep: $(wildcard $(SDIR)/*.hits)
	./beams_hits.awk $^ > $@

%.pene: %.edep ./pene-trigger.awk
	./pene-trigger.awk 'isPene(){print C, Pprim, Eprim, DIR, LET2, LET3, LET4, BGO1, BGO2}' $< > $@

%.stop2: %.edep ./stop2-trigger.awk
	./stop2-trigger.awk 'isStop2(){print C, Pprim, Eprim, DIR, LET2, LET3, LET4, BGO1, BGO2}' $< > $@

%.stop1: %.edep ./stop1-trigger.awk
	./stop1-trigger.awk 'isStop1(){print C, Pprim, Eprim, DIR, LET1, LET2, BGO1, BGO2}' $< > $@

%_e¯pα.2dhist: %_qn_e¯pα.bxes %_qs_e¯pα.bxes
	./bxes.awk boxes_stride=25 $^ > $@

%_e¯pα.2dhist: %_i2_e¯pα.bxes %_e2_e¯pα.bxes
	./bxes.awk boxes_stride=10 $^ > $@

%_stop2_e2_e¯pα.bxes: %.stop2 ./ahepam-pene2e2.bx
	grep '^[24]' $< | ./ahepam-pene2e2.bx --run > $@

%_stop2_i2_e¯pα.bxes: %.stop2 ./ahepam-pene2i2.bx
	grep '^[13]' $< | ./ahepam-pene2i2.bx --run > $@

%_pene_qs_e¯pα.bxes: %.pene ./ahepam-pene2qs.bx
	grep '^[24]' $< | ./ahepam-pene2qs.bx --run > $@

%_pene_qn_e¯pα.bxes: %.pene ./ahepam-pene2qn.bx
	grep '^[13]' $< | ./ahepam-pene2qn.bx --run > $@

%_pene_q1_e¯pα.bxes: %.pene ./ahepam-pene2q1.bx
	grep '^1' $< | ./ahepam-pene2q1.bx --run > $@

%_pene_q4_e¯pα.bxes: %.pene ./ahepam-pene2q4.bx
	grep '^3' $< | ./ahepam-pene2q4.bx --run > $@

%_pene_qs_e¯.gmap: %.pene ./ahepam-pene2qs.bx
	grep '^[24] e' $< | ./ahepam-pene2qs.bx --run --gnuplot > $@

%_pene_qs_p.gmap: %.pene ./ahepam-pene2qs.bx
	grep '^[24] p' $< | ./ahepam-pene2qs.bx --run --gnuplot > $@

%_pene_qs_α.gmap: %.pene ./ahepam-pene2qs.bx
	grep '^[24] α' $< | ./ahepam-pene2qs.bx --run --gnuplot > $@

%_pene_qs_e¯pα.gmap: %.pene ./ahepam-pene2qs.bx
	grep '^[24]' $< | ./ahepam-pene2qs.bx --run --gnuplot > $@

%_pene_q1_e¯.gmap: %.pene ./ahepam-pene2q1.bx
	grep '^1 e' $< | ./ahepam-pene2q1.bx --run --gnuplot > $@

%_pene_q1_p.gmap: %.pene ./ahepam-pene2q1.bx
	grep '^1 p' $< | ./ahepam-pene2q1.bx --run --gnuplot > $@

%_pene_q1_α.gmap: %.pene ./ahepam-pene2q1.bx
	grep '^1 α' $< | ./ahepam-pene2q1.bx --run --gnuplot > $@

%_pene_q1_e¯pα.gmap: %.pene ./ahepam-pene2q1.bx
	grep '^1' $< | ./ahepam-pene2q1.bx --run --gnuplot > $@

%_pene_q4_e¯.gmap: %.pene ./ahepam-pene2q4.bx
	grep '^3 e' $< | ./ahepam-pene2q4.bx --run --gnuplot > $@

%_pene_q4_p.gmap: %.pene ./ahepam-pene2q4.bx
	grep '^3 p' $< | ./ahepam-pene2q4.bx --run --gnuplot > $@

%_pene_q4_α.gmap: %.pene ./ahepam-pene2q4.bx
	grep '^3 α' $< | ./ahepam-pene2q4.bx --run --gnuplot > $@

%_pene_q4_e¯pα.gmap: %.pene ./ahepam-pene2q4.bx
	grep '^3' $< | ./ahepam-pene2q4.bx --run --gnuplot > $@

