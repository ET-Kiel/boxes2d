#! /usr/bin/gawk -i

BEGIN {
    mMIP      = 0.1
    lratio    = 3
    mELECTRON = 0.200
    xELECTRON = 4.000
    rELECTRON = 3/2
    mALPHA    = 1.500
    mPROTON   = 0.350
    xPROTON   = 0.850
}

function isStop2() {
    if ($8<mMIP || $9<mMIP) return 0
    DIR = -1
    if ($6>mMIP && $7>mMIP && $10<mMIP && $11<mMIP) DIR=0
    else if ($6<mMIP && $7<mMIP && $10>mMIP && $11>mMIP) DIR=1
    if (DIR<0) return 0
    if (!DIR) {
	LET2 = $6<$7 ? $6 : $7
	if (lratio*LET2 < $6 || lratio*LET2 < $7) return 0
	LET4 = ($10+$11)/2
	BGO1 = $14
	BGO2 = $15
    } else {
	LET2 = $10<$11 ? $10 : $11
	if (lratio*LET2 < $10 || lratio*LET2 < $11) return 0 
	LET4 = ($6+$7)/2
	BGO1 = $15
	BGO2 = $14
    }
    LET3 = $8<$9 ? $8 : $9
    Eprim = $2
    Pprim = $3
    if (LET2 < mELECTRON && LET3 < xELECTRON) {
	if (LET3 > rELECTRON*LET2) return C=2
	return C=4
    }
    if (LET2 > mALPHA && LET3 > mALPHA && LET3 > LET2) return C=3
    if (LET2 > mPROTON && LET3 > mPROTON && LET2 < xPROTON && LET3 > LET2) return C=1
    return C=5
}
