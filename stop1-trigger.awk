#! /usr/bin/gawk -i

BEGIN {
    mMIP      = 0.1
    lratio    = 3
    mELECTRON = 0.250
    mELECTRON2= 0.480
    mALPHA    = 3.600
}

function isStop1() {
    if ($8>mMIP || $9>mMIP) return 0
    DIR = -1
    if ($4>mMIP && $5>mMIP && $6>mMIP && $7>mMIP && $10<mMIP && $11<mMIP) DIR=0
    else if ($6<mMIP && $7<mMIP && $10>mMIP && $11>mMIP && $12>mMIP && $13>mMIP) DIR=1
    if (DIR<0) return 0
    if (!DIR) {
	LET2 = $6<$7 ? $6 : $7
	if (lratio*LET2 < $6 || lratio*LET2 < $7) return 0
	LET1 = ($4+$5)/2
	if (lratio*LET1 < $4 || lratio*LET1 < $5) return 0
	BGO1 = $14
	BGO2 = $15
    } else {
	LET2 = $10<$11 ? $10 : $11
	if (lratio*LET2 < $10 || lratio*LET2 < $11) return 0 
	LET1 = ($12+$13)/2
	if (lratio*LET1 < $12 || lratio*LET1 < $13) return 0
	BGO1 = $15
	BGO2 = $14
    }
    Eprim = $2
    Pprim = $3
    if (LET2<mELECTRON || LET1<mELECTRON) return C=2
    if (LET2<mELECTRON2 || LET1<mELECTRON2) return C=4
    if (LET1>mALPHA) return C=3
    return C=1
}
