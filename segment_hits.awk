#! /usr/bin/gawk -f


BEGIN {
    nDET=70
}

BEGINFILE {
    Pprim="?"
    if (FILENAME ~ /ele/) Pprim="e¯"
    if (FILENAME ~ /pro/) Pprim="p"
    if (FILENAME ~ /al/) Pprim="α"
}
ENDFILE { emit() }
$1 != evid { emit() }

function emit( p, d) {
    if (!evid) return
    if (Pprim=="?") {
	if (Particle["alpha"])       Pprim="α"
	else if (Particle["proton"]) Pprim="p"
	else if (Particle["e-"])     Pprim="e¯"
	else for (d in Particle)     Pprim=d
    }
    printf "%u %g %s %g %g ", evid, Eprim, Pprim, Theta, Phi
    for (d=0; d<nDET; d++) printf " %g", Edep[d]+0
    printf "\n"
    delete Edep
    delete Particle
    evid = 0
}

/^[0-9]/ {
    if ($3 > nDET) nDET = $3
    evid = $1
    Eprim = $4
    Edep[$3] += $5
    Theta = atan2(sqrt($10**2+$11**2), $12)
    Phi   = atan2($11, $10)
    Particle[$6] ++
}
