#! /usr/bin/gawk -f
BEGIN {
    ppart_stride = 4
    boxes_stride = 25
    log10 = log(10)
    resE = 0.01
    minlogE = 1
    wE = 1e6/log(2500)*log10
    wP = 1e6/log(5000)*log10
    wA = 1e6/log(2000)*log10
}

NF==13 {
    PP=0
    if (/ e¯ /) { PP=1; W = 1/wE/resE }
    if (/ p /)  { PP=2; W = 1/wP/resE  }
    if (/ α /)  { PP=3; W = 1/wA/resE  }
    if (!PP) next
    logEprim = log($7)/log10 - minlogE
    if (logEprim<0) logEprim = 0
    if (logEprim>=ppart_stride) logEprim = ppart_stride-resE
    BOX = $4
    CUT = $5
    if (!CUT) next
    if (CUT>maxCUT) maxCUT=CUT
    x = int(((PP-1)*ppart_stride + logEprim)/resE)
    y = BOX + boxes_stride*(CUT-1)
    #print CUT, BOX, PP, logEprim, x, y
    HIST[x,y] += W
}
END {
    nx = 3*ppart_stride/resE
    ny = maxCUT*boxes_stride
    printf "%d ", nx
    for (x=0; x<nx; x++) printf " %g", resE*x
    printf "\n"
    for (y=-1; y<ny; y++) {
	printf "%d ", y
	for (x=0; x<nx; x++) printf " %g", HIST[x,y]+0
	printf "\n"
    }
}
